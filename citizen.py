

from ast import If
from logger_default import custom_logger
import yaml
import pandas as pd
from google.cloud import firestore
db = firestore.Client()
logger = custom_logger(__name__).logger


class Helpers:
    def plus(val):
        if float(val) > 0:
            return ' +' + str(val)
        return str(val)

    def prettynum(val):
        if int(val) == val:
            return str(int(val))
        else:
            return(str(val))


class Credit:

    schema = yaml.safe_load(open("credits.yml"))['implemented']

    def get_credit_schema(schema):

        return {k: v['value'] for k, v in schema.items()}

    def get_df(schema):
        credit_schema_df = pd.DataFrame([schema]).T
        credit_schema_df = credit_schema_df[0].apply(pd.Series)
        credit_schema_df.index.name = 'action'
        credit_schema_df = credit_schema_df.sort_values(
            'value', ascending=False)

        credit_schema_df['value'] = credit_schema_df['value'].apply(
            Helpers.prettynum).apply(Helpers.plus)
        credit_schema_df = credit_schema_df.to_markdown(
            tablefmt='pretty', index=True)
        logger.info(credit_schema_df)
        return credit_schema_df

    credit_schema = get_credit_schema(schema)
    credit_schema_df = get_df(schema)

    db.collection('rules').document('bot').set(credit_schema)


class Citizen(Credit):

    def __init__(self, name, guild):

        self.name = str(name)
        self.guild = str(guild)
        self.nameguild = name + '_' + guild
        self.document = db.collection('users').document(self.nameguild)

    @property
    def doc(self):
        return self.document.get().to_dict()

    async def credit_event(self, event):

        if event in Citizen.credit_schema:
            self.document.set(
                {'name': self.name, 'guild': self.guild}, merge=True)
        else:
            logger.warning(f'event {event} not in schema!')
            return
        if event in ['jos', 'xi', 'grill']:
            self.document.set(
                {'name': self.name, 'guild': self.guild, event: 1}, merge=True)
        else:
            self.document.update({event: firestore.Increment(1)})

        logger.info(f'{self.name}, {event}')
        logger.info(self.doc)

    async def credit_report(self):

        doc = self.doc
        schema = Citizen.credit_schema
        df = pd.DataFrame(columns=['reason', 'multiplier', 'count', 'impact'])

        for k, v in doc.items():
            if k in schema:
                row = {'reason': k, 'count': v,
                       'multiplier': schema[k], 'impact': v * schema[k]}
                df = df.append(row, ignore_index=True)
        logger.info(df)
        score = df['impact'].sum()

        df['impact on score'] = df['impact'].apply(
            Helpers.prettynum).apply(Helpers.plus)
        df['multiplier'] = df['multiplier'].apply(Helpers.plus)
        print(df)
        df = df.sort_values('impact', ascending=False)
        df = df.drop(['impact', 'multiplier'], axis=1)
        self.report = df.to_markdown(index=False, tablefmt='pretty')
        self.score = round(score, 2)

    @staticmethod
    def verify_active(member, guild):

        active = db.collection('guilds').document(guild)
        logger.info(f"{active.get().to_dict().get(member,None)}")

        return active.get().to_dict().get(member, None)

    @staticmethod
    async def optin(member, guild):

        active = db.collection('guilds').document(guild)
        active.set({member: True}, merge=True)
        logger.info(f"{active.get().to_dict()}")

    @staticmethod
    async def optout(member, guild):

        active = db.collection('guilds').document(guild)
        active.set({member: False}, merge=True)
        logger.info(f"{active.get().to_dict()}")

        Citizen(member, guild).document.set({})


async def create_leaderboard(guild):
    docs = db.collection('users').where("guild", "==", guild).stream()

    df = pd.DataFrame(columns=['citizen', 'score'])
    for doc in docs:
        logger.info(f"{doc.id} => {doc.to_dict()}")

        citizen = Citizen(doc.to_dict()['name'], guild)

        await citizen.credit_report()

        df = df.append(
            {'citizen': citizen.name, 'score': citizen.score}, ignore_index=True)
    df = df.sort_values('score', ascending=False)
    df['score'] = df['score'].apply(Helpers.prettynum)
    return df.to_markdown(index=False, tablefmt='pretty')


if __name__ == '__main__':
    ...
