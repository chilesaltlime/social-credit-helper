from citizen import Citizen, create_leaderboard
import discord
from discord.ext import commands
from ast import alias
from discord.utils import get as discord_get
from pathlib import Path
from logger_default import custom_logger
logger = custom_logger(__name__).logger


class UserCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def get_user_credit(self, member):
        if not Citizen.verify_active(member.name, member.guild.name):
            return
        citizen = Citizen(member.name, member.guild.name)

        await citizen.credit_report()
        return citizen

    @commands.command(aliases=['score'])
    async def creditscore(self, ctx, *, member: discord.Member = None):
        member = member or ctx.author

        citizen = await self.get_user_credit(member)

        if citizen is None:
            await ctx.send(f"User `{member}` has not opted in to the social credit system. They can use the `~optin` command to join.")
            return

        await ctx.send(f"`{member}'s social credit score: {citizen.score}`")

    @commands.command(aliases=['report'])
    async def creditreport(self, ctx, *, member: discord.Member = None):

        member = member or ctx.author

        citizen = await self.get_user_credit(member)
        if citizen is None:
            await ctx.send(f"User `{member}` has not opted in to the social credit system. They can use the `~optin` command to join.")
            return

        await ctx.send(f"`{member}'s social credit report`")

        await ctx.send(f'`{citizen.report}`')
        await ctx.send(f'`Social credit score: {citizen.score}`')

    @commands.command(aliases=['lb'])
    async def leaderboard(self, ctx, *, member: discord.Member = None):
        """Says hello"""
        lb = await create_leaderboard(ctx.guild.name)
        await ctx.send(f'`{lb}`')

    @commands.command()
    async def rules(self, ctx, *, member: discord.Member = None):

        await ctx.send(f'`{Citizen.credit_schema_df}`')

    @commands.command()
    async def optin(self, ctx):
        await Citizen.optin(ctx.author.name, ctx.guild.name)
        await ctx.send(f'`{ctx.author} has opted in to the social credit monitoring service, their score can now change`')

    @commands.command()
    async def optout(self, ctx):
        await Citizen.optout(ctx.author.name, ctx.guild.name)
        await ctx.send(f'`{ctx.author} has opted out of the social credit monitoring service, their score is deleted forever`')


class Utils(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.has_role("admin")
    @commands.command()
    async def ping(self, ctx):
        await ctx.send("`Bot latency: {}s`".format(round(self.bot.latency, 2)))

    @commands.has_role("admin")
    @commands.command(aliases=["git", "branch"])
    async def get_active_branch_name(self, ctx):

        head_dir = Path(".") / ".git" / "HEAD"
        with head_dir.open("r") as f:
            content = f.read().splitlines()

        for line in content:
            if line[0:4] == "ref:":
                branch = line.partition("refs/heads/")[2]
                await ctx.send(f"""current branch: `{branch}`""")

    @commands.has_role("admin")
    @commands.command("clear")
    async def clear_text(self, ctx, *, line_count):

        logger.info(line_count)
        if int(line_count) < 50:
            await ctx.channel.purge(limit=int(line_count))
        elif int(line_count) >= 50:
            await ctx.send("`please try to delete in batches of 50 or less` :pray:")

    @commands.has_role("admin")
    @commands.command()
    async def sudooptin(self, ctx, *, member: discord.Member = None):
        await Citizen.optin(member.name, ctx.guild.name)
        await ctx.send(f'`{member.name} was opted in by @admin`')

    @commands.has_role("admin")
    @commands.command()
    async def sudooptout(self, ctx, *, member: discord.Member = None):
        await Citizen.optout(member.name, ctx.guild.name)
        await ctx.send(f'`{member.name} was opted out by @admin`')

    @commands.has_role("admin")
    @commands.command()
    async def change_credits(self, ctx, *, member: discord.Member = None, event):
        await Citizen(member.name, ctx.guild.name).credit_event(event)
        await ctx.send(f'`{member.name} was given action {event}`')
