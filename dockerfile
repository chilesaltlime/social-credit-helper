FROM python:3.9-slim

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN apt update 
RUN mkdir /opt/sc
COPY . /opt/sc/
ENV GOOGLE_APPLICATION_CREDENTIALS "/opt/sc/service-acct.json"
WORKDIR /opt/sc/
CMD python bot.py
