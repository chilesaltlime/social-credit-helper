
from discord.ext import commands
from discord.utils import get as discord_get
import discord
from citizen import Citizen
from logger_default import custom_logger
logger = custom_logger(__name__).logger


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):

        if message.mentions:
            if message.mentions[0].name != message.author.name:
                if Citizen.verify_active(message.mentions[0].name, message.guild.name):

                    await Citizen(message.mentions[0].name, message.guild.name).credit_event('mentioned')

        if not Citizen.verify_active(message.author.name, message.guild.name):
            return

        if message.author.bot:
            return

        await Citizen(message.author.name, message.guild.name).credit_event('posted')

        if 'xi' in message.author.name:
            await Citizen(message.author.name, message.guild.name).credit_event('xi')
        if 'grillionaire' in message.author.name:
            await Citizen(message.author.name, message.guild.name).credit_event('grill')
        if 'bowl cut jesus' in message.author.name:
            await Citizen(message.author.name, message.guild.name).credit_event('jos')
        if 'vaush' in message.content.lower():
            await Citizen(message.author.name, message.guild.name).credit_event('vaush')

        channel = message.channel
        channel_substrings = ['good-vibes', 'horny-jail', 'bloompill']
        for substr in channel_substrings:
            if substr in channel.name:
                await Citizen(message.author.name, message.guild.name).credit_event(substr.replace('-', '_'))

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):

        channel = self.bot.get_guild(
            payload.guild_id).get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        # if message.author.bot:
        #     return
        if not Citizen.verify_active(message.author.name, message.guild.name):
            return

        if payload.member == message.author:
            await Citizen(message.author.name, message.guild.name).credit_event('self_react')
            return
        emoji = payload.emoji.name
        logger.info(emoji)

        if 'kekw' in emoji:
            emoji = 'kekw'
        if 'chad' in emoji:
            emoji = 'chad'
        if emoji in ['thistbh', 'cringe', 'star', 'bonk', 'guillotine', 'kolibri', 'based', '👑', 'chad', 'mellow']:
            await Citizen(message.author.name, message.guild.name).credit_event(emoji)
