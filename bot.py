
import yaml
from discord.ext import commands, tasks
from discord.utils import get as discord_get
import asyncio
import json
import discord
from commands import Utils, UserCommands
from events import Events
from logger_default import custom_logger
logger = custom_logger(__name__).logger


intents = discord.Intents.default()
bot = commands.Bot(command_prefix="~", intents=intents)


intents.members = True
intents.reactions = True


async def status_loop():
    STATUS_LOOP = 480
    while True:
        await bot.change_presence(
            activity=discord.Game(name="optimizing your future")
        )
        await asyncio.sleep(STATUS_LOOP)


bot.remove_command("help")
task_wrapper = {"task": None}


@bot.event
async def on_ready():
    task_wrapper["task"] = bot.loop.create_task(status_loop())

    bot.add_cog(Utils(bot))
    bot.add_cog(Events(bot))
    bot.add_cog(UserCommands(bot))

    logger.info("Bot started.")
    logger.info("~~~~~~~~~~~~~~~~~~~~~~~~")


@bot.command(aliases=["manual", "commands", "info"])
async def help(ctx):
    embed = discord.Embed(title="Welcome comrade")

    embed.add_field(
        inline=False,
        name="Get social credit score",
        value="""
        `~creditscore`
        `~score`
            """
    )

    embed.add_field(
        inline=False,
        name="Get credit report",
        value="""
        `~creditreport`
        `~report`
            """
    )

    embed.add_field(
        inline=False,
        name="Get leaderboard",
        value="""
        `~leaderboard`
        `~lb`
            """
    )

    embed.add_field(
        inline=False,
        name="See point system",
        value="""
        `~rules`
            """
    )

    embed.add_field(
        inline=False,
        name="opt in to the social credit system. your score will change",
        value="""
        `~optin`
            """
    )

    embed.add_field(
        inline=False,
        name="opt out of the social credit system. your score will be deleted and not change until opt in",
        value="""
        `~optout`
            """
    )

    await ctx.send(embed=embed)


if __name__ == "__main__":
    logger.info(f"discord version {discord.__version__}")
    with open("tokens.json", "r") as tokens_file:
        tokens = json.load(tokens_file)
    bot.run(tokens["dev_token"])
